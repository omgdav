manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{omgdav}
  s.version = (ENV['VERSION'] || '0.0.4').dup
  s.authors = ["OMGDAV hackers"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{omgdav-public@bogomips.org}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://bogomips.org/omgdav/'
  s.summary = 'Rack app for exposing MogileFS via WebDAV'
  s.executables = %w(omgdav-setup omgdav-sync)
  s.test_files = Dir['test/test_*.rb']
  s.add_dependency('rack', ['>= 1.4', '< 3.0'])
  s.add_dependency('kcar', ['~> 0.4'])
  s.add_dependency('mogilefs-client', ['~> 3.4'])
  s.add_dependency('nokogiri', ['~> 1.5'])
  s.add_dependency('sequel', ['>= 4.37', '< 6.0'])
  s.required_ruby_version = '>= 2.3'
  s.add_development_dependency('unicorn', ['>= 4.4', '< 6.6.6'])
  s.add_development_dependency('minitest', '~> 5.0')
  s.licenses = %w(AGPL-3.0+)
end
