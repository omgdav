# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestMove < Minitest::Test
  include TestMogileFSIntegration

  def test_move_file
    # create a file
    resp = @req.put("/hello", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i
    resp = @req.put("/goodbye", input: StringIO.new("GOODBYE"))
    assert_equal 201, resp.status.to_i

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/bar',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("MOVE", "/hello", opts)
    assert_equal 201, status.to_i

    status, headers, body = req("GET", "/bar")
    assert_equal 200, status.to_i
    assert_equal "5", headers["Content-Length"]
    assert_equal "HELLO", body_string(body)

    assert_equal 404, req("GET", "/hello")[0].to_i

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/bar',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("MOVE", "/goodbye", opts)
    assert_equal 204, status.to_i
  end

  def test_move_collection
    assert_equal 201, req("MKCOL", "/a")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b")[0].to_i
    (0..9).each do |i|
      input = StringIO.new(i.to_s)
      assert_equal 201, @req.put("/a/#{i}", input: input).status.to_i
      input.rewind
      assert_equal 201, @req.put("/a/b/#{i}", input: input).status.to_i
    end

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/c',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("MOVE", "/a", opts)
    assert_equal 201, status.to_i
    (0..9).each do |i|
      assert_equal 404, @req.get("/a/#{i}").status.to_i
      assert_equal 404, @req.get("/a/b/#{i}").status.to_i

      status, _, body = req("GET", "/c/#{i}")
      assert_equal 200, status.to_i
      assert_equal i.to_s, body_string(body)

      status, _, body = req("GET", "/c/b/#{i}")
      assert_equal 200, status.to_i
      assert_equal i.to_s, body_string(body)
    end
  end
end
