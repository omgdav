# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "./test/integration"
require "unicorn"

LITMUS_DIR = ENV["LITMUS_DIR"]
class TestLitmus < Minitest::Test
  include TestMogileFSIntegration

  def setup
    super
    @uni_err = Tempfile.new(%w(stderr .log))
    @ru = Tempfile.new(%w(omgdav .ru))
    @ru.sync = true
    @ru.write <<EOF
require 'omgdav/app'
db = Sequel.connect("sqlite://#{@db_file.path}", synchronous: :off)
mogc =  MogileFS::MogileFS.new(hosts: #{@hosts.inspect}, domain: "testdom")
run OMGDAV::App.new(db, mogc)
EOF
    uni_sock = TCPServer.new(@test_host, 0)
    port = uni_sock.addr[1]
    @url = "http://#@test_host:#{port}/"
    @uni_pid = fork do
      ENV["UNICORN_FD"] = uni_sock.fileno.to_s
      ENV["RACK_ENV"] = "deployment"
      $stderr.reopen(@uni_err.path, "a")
      uni = Unicorn::HttpServer.new(@app, listeners: ["#@test_host:#{port}"])
      uni.start.join
      exit! 0
    end
    uni_sock.close
  end

  def test_litmus
    cmd = %W(#{ENV["MAKE"] || 'make'} -C #{LITMUS_DIR} URL=#@url check)
    out = Tempfile.new("err")
    err = Tempfile.new("out")
    pid = fork do
      $stdout.reopen(out)
      $stderr.reopen(err)
      exec(*cmd)
    end
    _, status = Process.waitpid2(pid)
    if $DEBUG || $VERBOSE
      out.rewind
      err.rewind
      puts out.read
      puts err.read
    end
    assert status.success?, status.inspect
  end

  def teardown
    Process.kill(:QUIT, @uni_pid)
    _, status = Process.waitpid2(@uni_pid)
    assert status.success?, status.inspect
    puts @uni_err.read if $DEBUG || $VERBOSE
    super
  end
end if LITMUS_DIR
