# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class Test_OMGDAV_App < Minitest::Test
  include TestMogileFSIntegration

  def test_basic
    # create a file
    resp = @req.put("/foo", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i

    # make sure it got into MogileFS
    info = @mogc.file_info("foo")
    assert_equal 5, info["length"]
    assert_equal "HELLO", @mogc.get_file_data("foo")

    # ensure we can get it back
    status, headers, body = req("GET", "/foo")
    assert_equal 200, status.to_i
    assert_equal "5", headers["Content-Length"]
    assert_equal "HELLO", body_string(body)
    mtime = Time.parse(headers["Last-Modified"])
    assert_nil body.close

    # ensure we can get a range back
    status, headers, body = req("GET", "/foo", "HTTP_RANGE" => "bytes=1-")
    assert_equal 206, status.to_i
    assert_equal "ELLO", body_string(body)
    assert_nil body.close

    status, headers, body = req("PROPFIND", "/foo", "HTTP_DEPTH" => "0")
    assert_equal 207, status.to_i
    assert_match(%r{\Atext/xml\b}, headers["Content-Type"])
    foo0 = body_string(body)

    status, headers, body = req("PROPFIND", "/foo", "HTTP_DEPTH" => "1")
    assert_equal 207, status.to_i
    assert_match(%r{\Atext/xml\b}, headers["Content-Type"])
    foo1 = body_string(body)
    assert_equal foo0, foo1

    # ensure we can get a PROPFIND with Depth: 0
    status, headers, body = req("PROPFIND", "/", "HTTP_DEPTH" => "0")
    assert_equal 207, status.to_i
    xml = Nokogiri::XML(body_string(body))
    responses = xml.search("//D:multistatus/D:response")
    assert_equal 1, responses.size
    root = responses[0]
    assert_equal "/", root.search("//D:href").text
    assert_equal 1, root.search(".//D:collection").size

    # PROPFIND with Depth:1
    status, headers, body = req("PROPFIND", "/", "HTTP_DEPTH" => "1")
    assert_equal 207, status.to_i
    xml = Nokogiri::XML(body = body_string(body))
    responses = xml.search("//D:multistatus/D:response")
    assert_equal 2, responses.size
    assert_equal root.to_s, responses[0].to_s
    foo = responses[1]
    refute_equal foo.to_s, root.to_s
    assert_equal "/foo", foo.search(".//D:href").text
    clen = foo.search(".//lp1:getcontentlength", "lp1" => "DAV:")
    assert_equal '5', clen.text
    modified = foo.search(".//lp1:getlastmodified", "lp1" => "DAV:")
    assert_equal mtime, Time.httpdate(modified.text)
    created= foo.search(".//lp1:creationdate", "lp1" => "DAV:")
    assert_equal mtime, Time.xmlschema(created.text)
    assert_empty foo.search(".//D:collection")

    # PROPFIND with Depth:infinity
    status, headers, body = req("PROPFIND", "/", "HTTP_DEPTH" => "infinity")
    assert_equal 400, status.to_i

    # PROPFIND with unspecified Depth (assumed infinity)
    status, headers, body = req("PROPFIND", "/")
    assert_equal 400, status.to_i
  end

  def test_deep_delete
    assert_equal 201, req("MKCOL", "/a")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b/c")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b/c/d")[0].to_i
    range = (0..9)
    range.each do |i|
      resp = @req.put("/a/b/c/d/#{i}", input: StringIO.new("#{i}"))
      assert_equal 201, resp.status.to_i
      assert @mogc.exist?("a/b/c/d/#{i}")

      resp = @req.put("/a/#{i}", input: StringIO.new("#{i}"))
      assert_equal 201, resp.status.to_i
      assert @mogc.exist?("a/#{i}")
    end
    assert_equal 204, req("DELETE", "/a")[0].to_i
    range.each do |i|
      refute @mogc.exist?("a/b/c/d/#{i}")
      refute @mogc.exist?("a/#{i}")
    end
    assert_equal [ @app.root_node ], @db[:paths].to_a
  end
end
