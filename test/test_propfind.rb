# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestPropfind < Minitest::Test
  include TestMogileFSIntegration

  def test_invalid_input
    opts = {
      "HTTP_DEPTH" => "0",
      input: StringIO.new("<foo>bar")
    }
    assert_equal 400, req("PROPFIND", "/", opts)[0].to_i
  end

  def test_dir
    opts = { "HTTP_DEPTH" => "0" }
    assert_equal 201, req("MKCOL", "/a")[0].to_i

    status, _, body = req("PROPFIND", "/a", opts)
    assert_equal 207, status.to_i
    a0 = body_string(body)

    opts["HTTP_DEPTH"] = "1"
    status, _, body = req("PROPFIND", "/a", opts)
    assert_equal 207, status.to_i
    a1 = body_string(body)
    assert_equal a0, a1
  end
end
