# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestCopy < Minitest::Test
  include TestMogileFSIntegration

  def test_copy_file
    # create a file
    resp = @req.put("/hello", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i

    resp = @req.put("/goodbye", input: StringIO.new("GOODBYE"))
    assert_equal 201, resp.status.to_i

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/bar',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("COPY", "/hello", opts)
    assert_equal 201, status.to_i

    status, _, body = req("GET", "/bar")
    assert_equal 200, status.to_i
    assert_equal "HELLO", body_string(body)

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/bar',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("COPY", "/goodbye", opts)
    assert_equal 204, status.to_i

    status, _, body = req("GET", "/bar")
    assert_equal 200, status.to_i
    assert_equal "GOODBYE", body_string(body)

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/bar',
      "HTTP_HOST" => 'example.com',
    }
    status, _, _ = req("COPY", "/bar", opts)
    assert_equal 403, status.to_i

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/a/b',
      "HTTP_HOST" => 'example.com:80',
    }
    status, _, _ = req("COPY", "/bar", opts)
    assert_equal 409, status.to_i

    assert_equal 201, req("MKCOL", "/a")[0].to_i

    opts = {
      "HTTP_DESTINATION" => 'http://example.com/a/b',
      "HTTP_HOST" => 'example.com:80',
    }
    status, _, _ = req("COPY", "/bar", opts)
    assert_equal 201, status.to_i
    status, _, body = req("GET", "/a/b")
    assert_equal 200, status.to_i
    assert_equal "GOODBYE", body_string(body)

    opts = {
      'HTTP_DESTINATION' => 'http://example.com/a/b',
      'HTTP_HOST' => 'example.com',
      'HTTP_OVERWRITE' => 'F',
    }
    status, _, _ = req("COPY", "/hello", opts)
    assert_equal 412, status.to_i
    status, _, body = req("GET", "/a/b")
    assert_equal 200, status.to_i
    assert_equal "GOODBYE", body_string(body)

    opts = {
      'HTTP_DESTINATION' => 'http://www.example.com/a/b',
      'HTTP_HOST' => 'example.com',
    }
    assert_equal 502, req("COPY", "/hello", opts)[0].to_i

    opts = {
      'HTTP_DESTINATION' => 'http://example.com/a/b',
      'HTTP_HOST' => 'example.com:8080',
    }
    assert_equal 502, req("COPY", "/hello", opts)[0].to_i
  end

  def test_copy_collection_empty
    assert_equal 201, req("MKCOL", "/a")[0].to_i

    opts = {
      'HTTP_DESTINATION' => 'http://example.com/b',
      'HTTP_HOST' => 'example.com',
    }
    assert_equal 201, req("COPY", "/a", opts)[0].to_i
    assert @db[:paths][name: "b"][:collection]

    assert_equal 204, req("COPY", "/a", opts)[0].to_i
  end

  def test_copy_collection_full
    assert_equal 201, req("MKCOL", "/a")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b")[0].to_i
    assert_equal 201, @req.put("/a/c", input: StringIO.new("a")).status.to_i
    assert_equal 201, @req.put("/a/b/c", input: StringIO.new("C")).status.to_i

    opts = {
      'HTTP_DESTINATION' => 'http://example.com/b',
      'HTTP_HOST' => 'example.com',
    }
    assert_equal 201, req("COPY", "/a", opts)[0].to_i

    status, _, body = req("GET", "/b/c")
    assert_equal 200, status.to_i
    assert_equal "a", body_string(body)

    status, _, body = req("GET", "/b/b/c")
    assert_equal 200, status.to_i
    assert_equal "C", body_string(body)
  end
end
