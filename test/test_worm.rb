# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestWorm < Minitest::Test
  include TestMogileFSIntegration

  def test_worm
    resp = @req.put("/foo", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i

    @app = OMGDAV::App.new(@db, @mogc, methods: :worm)
    @req = Rack::MockRequest.new(@app)
    resp = @req.put("/foo", input: StringIO.new("GOODBYE"))
    assert_equal 409, resp.status.to_i

    resp = @req.put("/bar", input: StringIO.new("GOODBYE"))
    assert_equal 201, resp.status.to_i

    status, _, body = req("GET", "/foo")
    assert_equal 200, status.to_i
    assert_equal "HELLO", body_string(body)

    assert_equal 405, req("DELETE", "/foo")[0].to_i
  end
end
