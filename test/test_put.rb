# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestPut < Minitest::Test
  include TestMogileFSIntegration

  def test_put_overwrite
    resp = @req.put("/hello", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i
    before = @db[:paths].to_a[0]

    status, headers, body = req("GET", "/hello")
    assert_equal 200, status.to_i
    assert_equal "5", headers["Content-Length"]
    assert_equal "HELLO", body_string(body)

    resp = @req.put("/hello", input: StringIO.new("GOODBYE"))
    assert_equal 204, resp.status.to_i
    after = @db[:paths].to_a[0]

    status, headers, body = req("GET", "/hello")
    assert_equal 200, status.to_i
    assert_equal "7", headers["Content-Length"]
    assert_equal "GOODBYE", body_string(body)

    [ :parent_id, :id, :domain_id, :name ].each do |field|
      assert_equal before[field], after[field]
    end
  end
end
