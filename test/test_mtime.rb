# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestMtime < Minitest::Test
  include TestMogileFSIntegration

  def test_mtime
    assert_equal 201, req("MKCOL", "/a")[0].to_i
    assert_equal 201, req("MKCOL", "/a/b")[0].to_i
    now = Time.now
    before = @db[:paths].where(name: "a").to_a
    assert_equal before.size, 1
    before_mtime = Time.at(before[0][:mtime])
    assert_in_delta before_mtime, now, 2

    # creating a new directory sets mtime in parent (only), not grandparent
    sleep 1
    assert_equal 201, req("MKCOL", "/a/b/c")[0].to_i
    after = @db[:paths].where(name: "a").to_a
    assert_equal after.size, 1
    after_mtime = Time.at(after[0][:mtime])
    assert_equal before_mtime, after_mtime

    # creating a new file sets mtime in parent (only), not grandparent
    sleep 1
    resp = @req.put("/a/b/f", input: StringIO.new("HELLO"))
    assert_equal 201, resp.status.to_i
    after = @db[:paths].where(name: "a").to_a
    assert_equal after.size, 1
    after_mtime = Time.at(after[0][:mtime])
    assert_equal before_mtime, after_mtime

    b = @db[:paths].where(name: "b").to_a
    f = @db[:paths].where(name: "f").to_a
    assert_equal b.size, 1
    assert_equal f.size, 1
    assert_equal b[0][:mtime], f[0][:mtime]

    # updating existing file should not change mtime
    sleep 1
    resp = @req.put("/a/b/f", input: StringIO.new("HELLO"))
    assert_equal 204, resp.status.to_i
    f = @db[:paths].where(name: "f").to_a
    assert_equal f.size, 1
    assert_operator f[0][:mtime], :>, b[0][:mtime]

    bb = @db[:paths].where(name: "b").to_a
    assert_equal bb.size, 1
    assert_equal bb[0][:mtime], b[0][:mtime]
  end
end
