# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
require 'rack/builder'
require 'rack/lobster'

# ensure everything works when we're using Rack::URLMap with builder
class TestUrlmap < Minitest::Test
  include TestMogileFSIntegration

  def test_url_map
    old_app = @app
    @app = Rack::Builder.new do
      map "/test" do
        run old_app
      end
      map "http://example.com/test" do
        run old_app
      end
      map "/bogus" do
        run Rack::Lobster.new
      end
    end.to_app

    assert_equal 201, req("PUT", "/test/foo", input: StringIO.new("H"))[0].to_i

    assert_equal "foo", @db[:paths].where { self.~(parent_id: 0) }.first[:name]
    status, headers, body = req("PROPFIND", "/test", "HTTP_DEPTH" => "1")
    xml = Nokogiri.XML(body)
    hrefs = xml.search("//D:href").to_a.map { |x| x.text }
    assert_equal 2, hrefs.size
    assert hrefs.include?("/test/"), hrefs.inspect
    assert hrefs.include?("/test/foo"), hrefs.inspect

    assert_equal 200, req("HEAD", "/test/foo")[0].to_i

    opts = {
      "HTTP_DESTINATION" => "http://example.com/test/bar",
      "HTTP_HOST" => "example.com",
    }
    assert_equal 201, req("MOVE", "/test/foo", opts)[0].to_i
    opts["HTTP_DESTINATION"] = "http://example.com/bar"
    assert_equal 502, req("MOVE", "/test/bar", opts)[0].to_i

    opts["HTTP_DESTINATION"] = "http://example.com/test/foo"
    assert_equal 201, req("COPY", "/test/bar", opts)[0].to_i

    assert_equal 201, req("MKCOL", "/test/col")[0].to_i
    opts["HTTP_DESTINATION"] = "http://example.com/test/col/foo"
    assert_equal 201, req("COPY", "/test/bar", opts)[0].to_i

    opts["HTTP_DESTINATION"] = "http://example.com/zz"
    assert_equal 502, req("MOVE", "/test/col/", opts)[0].to_i

    opts["HTTP_DESTINATION"] = "http://example.com/test/COL"
    assert_equal 201, req("MOVE", "/test/col/", opts)[0].to_i
  end
end
