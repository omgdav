# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestProppatch < Minitest::Test
  include TestMogileFSIntegration

  def test_proppatch_live
    assert_equal 201, req("MKCOL", "/prop")[0].to_i

    t = Time.at(666)
    xml = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<D:propertyupdate xmlns:D="DAV:">
  <D:set>
    <D:prop>
      <getlastmodified xmlns="DAV:">#{t.httpdate}</getlastmodified>
    </D:prop>
  </D:set>
</D:propertyupdate>
    EOF
    opts = {
      input: StringIO.new(xml.strip!)
    }

    assert_equal 200, req("PROPPATCH", "/prop", opts)[0].to_i
    assert_equal 666, @db[:paths][name: "prop"][:mtime]
  end

  def test_getcontenttype
    path = "//D:multistatus/D:response/D:propstat/D:prop/D:getcontenttype"
    assert_equal 201, req("PUT", "/type")[0].to_i
    _, _, body = req("PROPFIND", "/type", "HTTP_DEPTH" => '0')
    t = Nokogiri::XML(body).search(path)
    assert_equal "application/octet-stream", t.text

    xml = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<D:propertyupdate xmlns:D="DAV:">
  <D:set>
    <D:prop>
      <getcontenttype xmlns="DAV:">audio/flac</getcontenttype>
    </D:prop>
  </D:set>
</D:propertyupdate>
    EOF
    opts = {
      input: StringIO.new(xml.strip!)
    }

    assert_equal 200, req("PROPPATCH", "/type", opts)[0].to_i
    _, _, body = req("PROPFIND", "/type", "HTTP_DEPTH" => '0')
    t = Nokogiri::XML(body).search(path)
    assert_equal "audio/flac", t.text

    xml = <<-EOF
<?xml version="1.0" encoding="utf-8" ?>
<D:propertyupdate xmlns:D="DAV:"
       xmlns:Z="http://ns.example.com/standards/z39.50/">
 <D:remove>
   <D:prop><D:getcontenttype/></D:prop>
 </D:remove>
</D:propertyupdate>
    EOF
    opts = {
      input: StringIO.new(xml.strip!)
    }
    assert_equal 200, req("PROPPATCH", "/type", opts)[0].to_i
    _, _, body = req("PROPFIND", "/type", "HTTP_DEPTH" => '0')
    t = Nokogiri::XML(body).search(path)
    assert_equal "application/octet-stream", t.text

    opts = {
      "HTTP_DESTINATION" => "http://example.com/type.txt",
      "HTTP_HOST" => "example.com",
    }
    assert_equal 201, req("MOVE", "/type", opts)[0].to_i
    _, _, body = req("PROPFIND", "/type.txt", "HTTP_DEPTH" => '0')
    t = Nokogiri::XML(body).search(path)
    assert_equal "text/plain", t.text

    # try something invalid
    [
      "audio flac",
      "foo/bar adlkj",
      "foo/bar paramet()=ff"
    ].each do |bad|
      xml = <<-EOF
  <?xml version="1.0" encoding="utf-8"?>
  <D:propertyupdate xmlns:D="DAV:">
    <D:set>
      <D:prop>
        <getcontenttype xmlns="DAV:">#{bad}</getcontenttype>
      </D:prop>
    </D:set>
  </D:propertyupdate>
      EOF
      opts = { input: StringIO.new(xml.strip!) }
      assert_equal 400, req("PROPPATCH", "/type.txt", opts)[0].to_i, bad
      assert_nil @db[:paths][name: "type.txt"][:contenttype]
    end

    xml = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<D:propertyupdate xmlns:D="DAV:">
  <D:set>
    <D:prop>
      <getcontenttype xmlns="DAV:">teXt/plain;  chArset="ASCII"</getcontenttype>
    </D:prop>
  </D:set>
</D:propertyupdate>
    EOF
    opts = { input: StringIO.new(xml.strip!) }
    assert_equal 200, req("PROPPATCH", "/type.txt", opts)[0].to_i
    i = @db[:paths][name: "type.txt"][:contenttype]
    assert_kind_of Integer, i
    t = @db[:prop_mappings][id: i][:value]
    assert_equal 'text/plain; charset="ASCII"', t
    refute_equal i, @db[:prop_mappings][value: 'audio/flac'][:id]
  end

  def test_proppatch_dead
    assert_equal 201, req("MKCOL", "/prop")[0].to_i

    xml = <<-EOF
<?xml version="1.0" encoding="utf-8" ?>
<D:propertyupdate xmlns:D="DAV:"
       xmlns:Z="http://ns.example.com/standards/z39.50/">
 <D:set>
   <D:prop>
     <Z:Authors>
       <Z:Author>Jim Whitehead</Z:Author>
       <Z:Author>Roy Fielding</Z:Author>
     </Z:Authors>
   </D:prop>
 </D:set>
 <D:remove>
   <D:prop><Z:Copyright-Owner/></D:prop>
 </D:remove>
</D:propertyupdate>
    EOF
    opts = {
      input: StringIO.new(xml.strip!)
    }

    assert_equal 200, req("PROPPATCH", "/prop", opts)[0].to_i
    prop = @db[:paths][name: "prop"]
    props = @app.dead_props_get(prop)
    ns_prop = props["http://ns.example.com/standards/z39.50/"]
    assert_instance_of Hash, ns_prop
    authors = ns_prop["Authors"].strip
    expect = "<Author>Jim Whitehead</Author>\n" \
             "       <Author>Roy Fielding</Author>"
    assert_equal expect, authors
  end
end
