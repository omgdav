# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "./test/integration"
require "omgdav/sync"

class TestSync < Minitest::Test
  include TestMogileFSIntegration

  def test_sync
    nr = 0
    (5..11).each do |i|
      (995..1005).each do |j|
        @mogc.store_content("#{i}/#{j}", "default", "#{i}-#{j}")
        @mogc.store_content("#{j}/#{i}", "default", "#{j}-#{i}")
        nr += 2
      end
    end
    imp = OMGDAV::Sync.new(@db, @mogc)
    imp.sync
    paths = @db[:paths].where { self.>(:parent_id, 0) }
    assert_equal nr, paths.where(collection: false).count
    assert_equal 18, paths.where(collection: true).count
    keys, _ = @mogc.list_keys
    cache = {}
    pkeys = []
    paths.where(collection: false).each do |node|
      pkeys << @app.node_to_key(node, cache)
    end
    assert_equal keys.sort, pkeys.sort

    # ensure MogileFS-only deletions are synced
    first = keys.shift
    @mogc.delete(first)
    imp.sync

    pkeys.clear
    cache.clear
    paths.where(collection: false).each do |node|
      pkeys << @app.node_to_key(node, cache)
    end
    assert_equal(keys.size, pkeys.size)
    refute pkeys.include?(first)

    # nuke it all!
    keys.each { |key| @mogc.delete(key) }
    imp.sync
    assert_equal 0, @db[:paths].where(collection: false).count
    refute_equal 0, @db[:paths].where(collection: true).count
  end
end
