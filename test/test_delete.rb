# -*- encoding: binary -*-
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require './test/integration'
class TestDelete < Minitest::Test
  include TestMogileFSIntegration

  def test_delete
    status, _, _ = req("DELETE", "/non-existent")
    assert_equal 404, status.to_i

    status, _, _ = req("DELETE", "/")
    assert_equal 403, status.to_i
  end
end
