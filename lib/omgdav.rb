# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "tempfile"
require 'time'
require 'uri'
require 'nokogiri'
require 'json'
require 'sequel'
require 'mogilefs'
require 'rack'
require 'rack/request'
# :startdoc:

module OMGDAV
  # :stopdoc:
  TypeConflict = Class.new(TypeError)
  InvalidContentType = Class.new(ArgumentError)
  BadResponse = Class.new(RuntimeError)

  LP2 = "http://apache.org/dav/props/"
  # :startdoc:
end
# :enddoc:
require "omgdav/version"
