# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# :enddoc:
Sequel.migration do
  up do
    alter_table(:paths) do
      add_column :contenttype, Integer, null: true, default: nil
    end
  end
  down do
    alter_table(:paths) do
      drop_column :contenttype
    end
  end
end
