# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# :enddoc:
Sequel.migration do
  up do
    create_table(:synctmp) do
      primary_key :id, type: :Bignum
    end
  end

  down do
    drop_table(:synctmp)
  end
end
