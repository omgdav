# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# :enddoc:
Sequel.migration do
  up do
    create_table(:paths) do
      primary_key :id, type: :Bignum
      Integer :domain_id, null: false # MogileFS domain
      TrueClass :collection, default: false
      Bignum :parent_id, null: false # parent collection
      Bignum :mtime, null: false
      Bignum :created, null: false
      Bignum :length, null: true
      TrueClass :executable, null: false, default: false
      String :name, null: false
      String :dead_props, null: true, text: true
      unique [ :domain_id, :parent_id, :name ]
    end

    create_table(:domains) do
      primary_key :id
      String :domain, null: false, unique: true
    end

    create_table(:prop_mappings) do
      primary_key :id
      String :value, null: false, unique: true
    end

    create_table(:copymove_locks) do
      Integer :domain_id, null: false
      Bignum :src_id, null: false
      Bignum :dst_id, null: false
      Bignum :lock_expire, null: false
      unique [ :domain_id, :src_id, :dst_id ]
    end
  end

  down do
    drop_table(:paths)
    drop_table(:domains)
    drop_table(:prop_mappings)
    drop_table(:copymove_locks)
  end
end
