# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/db"

# each of these is an sync job
class OMGDAV::Sync # :nodoc:
  include OMGDAV::DB

  def initialize(db, mogc, prefix = nil)
    @db = db
    @mogc = mogc
    @prefix = prefix
    @domain_id = ensure_domain(@mogc.domain)
  end

  def bad_key(key, msg) # :nodoc:
    warn "key=#{key.inspect} #{msg}"
  end

  def info_import(info, seen) # :nodoc:
    key = info["key"]
    return bad_key(key, "may not have `//'") if %r{//} =~ key
    return bad_key(key, "may not start with `/'") if %r{\A/} =~ key
    return bad_key(key, "may not have a NUL byte") if %r{\0} =~ key

    key.force_encoding(Encoding::UTF_8)
    return bad_key(key, "is not valid UTF-8") unless key.valid_encoding?

    parts = key.split('/'.freeze)

    dot = parts.grep(/\A(?:\.\.|\.)\z/)[0]
    return bad_key(key, "may not contain `..' or `.' as a component") if dot

    filename = parts.pop
    full_col = parts.join('/'.freeze)

    begin
      unless parent_id = seen[full_col]
        parent_id = root_node[:id]
        parts.each do |colname|
          col = col_ensure(parent_id, colname)
          parent_id = col[:id]
        end
        seen[full_col] = parent_id
      end

      file_ensure(parent_id, filename, info)
    rescue OMGDAV::TypeConflict
      bad_key(key, "path conflicts with existing collection or file")
    end
  end

  def sync # :nodoc:
    synctmp = @db[:synctmp]
    seen = {} # optimization for keys with many path elements

    # snapshot all existing path ids into synctmp table
    synctmp.delete
    path_ids = @db[:paths].select(:id).where(collection: false)
    @db["INSERT INTO synctmp(id) #{path_ids.sql}"].insert

    pd = synctmp.where(id: :$i).prepare(:delete, :delete_by_id, id: :$i)

    @mogc.each_file_info(@prefix) do |info|
      # delete valid nodes from synctmp as we iterate
      node = info_import(info, seen) and pd.call(i: node[:id])

      # don't let a pathological case OOM us
      seen.clear if seen.size > 1000
    end

    # any ids leftover in the synctmp table are stale
    @db["DELETE FROM paths WHERE id IN (SELECT id FROM synctmp)"].delete
  ensure
    synctmp.delete # cleanup
  end
end
