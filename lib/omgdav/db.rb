# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav"
module OMGDAV::DB

  # returns a domain ID
  def ensure_domain(domain)
    domains = @db[:domains]
    @db.transaction do
      q = { domain: domain }
      dom = domains[q]
      dom ? dom[:id] : domains.insert(q)
    end
  end

  def node_merge_info(node, info)
    info or return
    node[:length] = info["length"]
  end

  def node_update(node, info = nil)
    node_merge_info(node, info)
    mtime = node[:mtime] = mtime_now
    node_id = node.delete(:id)
    @db[:paths].where(id: node_id).update(node)

    node[:id] = node_id
  end

  def node_update_maybe(node, info)
    info or return
    if info && node[:length] != info["length"]
      node_update(node)
    end
  end

  def node_ensure(parent_id, name, info = nil)
    q = { name: name, domain_id: @domain_id, parent_id: parent_id }
    collection = info ? false : true

    node = @db[:paths][q]
    if node
      # node already exists
      raise OMGDAV::TypeConflict if node[:collection] != collection
      node_update_maybe(node, info)
    else
      # brand new node
      new = false
      node = q.dup
      node[:created] = node[:mtime] = mtime_now
      node[:collection] = collection
      node_merge_info(node, info)
      begin
        node[:id] = @db[:paths].insert(node)
        new = true
      rescue Sequel::DatabaseError
        # we may conflict on insert if we didn't use a transaction
        raise if @db.in_transaction?
        node = @db[:paths][q] or raise
        raise OMGDAV::TypeConflict if node[:collection] != collection
      end
      update_parent_mtime(node) if new
    end

    node
  end

  # ensures the given args maps to a directory, returns a hash for the row
  def col_ensure(parent_id, colname)
    node_ensure(parent_id, colname)
  end

  # ensures the given args maps to a file, returns a hash for the row
  def file_ensure(parent_id, filename, info)
    node_ensure(parent_id, filename, info)
  end

  def update_parent_mtime(node, mtime = nil)
    @db[:paths].where(id: node[:parent_id]).update(mtime: mtime || node[:mtime])
  end

  def node_lookup(parent_id, name)
    @db[:paths][name: name, domain_id: @domain_id, parent_id: parent_id]
  end

  def node_to_parts(node, cache)
    root_id = root_node[:id]
    return [] if root_id == node[:id] # special case

    orig_name = node[:name]
    cache_key = node[:parent_id]
    unless parts = cache[cache_key]
      parts = []
      until root_id == node[:parent_id]
        node = @db[:paths][id: node[:parent_id], domain_id: @domain_id] or break
        parts.unshift(node[:name].dup.freeze)
      end

      cache[cache_key] = parts.freeze
    end

    parts += [ orig_name ]
  end

  def node_to_key(node, cache)
    node_to_parts(node, cache).join('/'.freeze)
  end

  def col_resolve(parts)
    node = root_node
    parts.each do |colpart|
      node = node_lookup(node[:id], colpart)
      return unless node
      raise OMGDAV::TypeConflict unless node[:collection]
    end
    raise OMGDAV::TypeConflict unless node[:collection]
    node
  end

  def node_resolve(parts)
    return root_node if parts.empty?
    parts = parts.dup
    basename = parts.pop or return

    parent = col_resolve(parts) or return
    node_lookup(parent[:id], basename)
  end

  def file_resolve(parts)
    node = node_resolve(parts) or return
    raise OMGDAV::TypeConflict if node[:collection]
    node
  end

  def col_vivify(parts)
    col = root_node
    parts.each do |colname|
      col = col_ensure(col[:id], colname)
    end
    col
  end

  def node_delete(node)
    rc = @db[:paths].where(id: node[:id]).delete
    if rc != 1
      key = node_to_key(node, {}).inspect
      warn "expected rc=1 for delete(id=#{node[:id]}) (key=#{key}) got rc=#{rc}"
    end
    rc
  end

  def dead_props_get(node)
    db_props = node[:dead_props] or return
    db_props = JSON.parse(db_props)
    mapping = {}
    @db[:prop_mappings].each { |row| mapping[row[:id]] = row[:value] }
    raw_props = {}
    db_props.each do |ns, hash|
      tmp = raw_props[mapping[ns.to_i] || ns] = {}
      hash.each { |i, value| tmp[mapping[i.to_i] || i] = value }
    end
    raw_props
  end

  # returns the new property ID for a given value
  def prop_mapping_ensure(value)
    tbl = @db[:prop_mappings]
    q = { value: value }
    tbl.insert(q)
  rescue Sequel::Database::Error
    raise if @db.in_transaction?
    mapping = tbl[q] or raise
    mapping[:id]
  end

  def dead_props_set(node, raw_props)
    mapping = {}
    @db[:prop_mappings].each { |row| mapping[row[:value]] = row[:id] }
    db_props = {}
    raw_props.each do |ns, hash|
      ns_id = mapping[ns] ||= prop_mapping_ensure(ns)
      tmp = db_props[ns_id] = {}
      hash.each do |key,value|
        key_id = mapping[key] ||= prop_mapping_ensure(key)
        tmp[key_id] = value
      end
    end
    node[:dead_props] = db_props.to_json
  end

  def root_node
    q = @root_node and return q
    # root node always has parent_id:0
    q = {
      parent_id: 0,
      collection: true,
      name: ''.freeze,
      domain_id: @domain_id
    }
    node = @db[:paths][q] and return (@root_node = node)
    q[:mtime] = q[:created] = mtime_now
    begin
      q[:id] = @db[:paths].insert(q)
      q
    rescue Sequel::DatabaseError
      # we may conflict on insert if we didn't use a transaction
      raise if @db.in_transaction?
      @root_node = @db[:paths][q] or raise
    end
  end

  # returns the mime type based on key name
  def key_mime_type(key) # :nodoc:
    /(\.[^.]+)\z/ =~ key
    Rack::Mime.mime_type($1) # nil => 'application/octet-stream'
  end

  def content_type(node, cache = nil)
    if ctid = node[:contenttype]
      if row = @db[:prop_mappings][id: ctid]
        type = row[:value]
      else
        warn "DB consistency error: no property mapping for #{ctid.inspect}"
        type = key_mime_type(node[:name])
      end
      cache ? (cache[ctid] = type) : type
    else
      key_mime_type(node[:name])
    end
  end

  def check_attr!(name, value)
    # TODO: check compliance, erring on the side of being too strict here
    /\A[\w\.\+-]+\z/ =~ value or
      raise OMGDAV::InvalidContentType, "#{name}=#{value.inspect} invalid"
  end

  def content_type_id(value)
    # normalize the content type
    type_subtype, parameter = value.split(/;\s+/, 2)
    type, subtype = type_subtype.downcase.split('/'.freeze, 2)
    check_attr!(:type, type)
    check_attr!(:subtype, subtype)
    value = "#{type}/#{subtype}"

    if parameter
      attr_key, attr_val = parameter.split('='.freeze, 2)
      attr_val or
        raise OMGDAV::InvalidContentType,
              "parameter=#{parameter.inspect} invalid"
      attr_key.downcase!
      check_attr!(:parameter, attr_key)
      value << "; #{attr_key}=#{attr_val}"
    end

    row = @db[:prop_mappings][value: value] and return row[:id]
    prop_mapping_ensure(value)
  end

  def mtime_now
    Process.clock_gettime(Process::CLOCK_REALTIME, :second)
  end
end
