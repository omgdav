# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Options
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def call_options(env)
    resp = r(200)
    if %r{\A/} =~ env["PATH_INFO"]
      resp[1]["DAV"] = "1"
    end
    resp
  ensure
    drain_input(env)
  end
end
