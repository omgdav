# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav"
require "rack"
require "rack/utils"
require "rack/mime"
require "logger"
module OMGDAV::RackUtil

  def logger(env)
    env["rack.logger"] || Logger.new($stderr)
  end

  # returns a plain-text HTTP response
  def r(code, msg = nil, env = nil) # :nodoc:
    if env
      logger(env).warn("#{env['REQUEST_METHOD']} #{env['PATH_INFO']} " \
                       "#{code} #{msg.inspect}")
    end

    if Rack::Utils::STATUS_WITH_NO_ENTITY_BODY.include?(code)
      [ code, {}, [] ]
    else
      msg ||= Rack::Utils::HTTP_STATUS_CODES[code] || ""

      if msg.size > 0
        # using += to not modify original string (owned by Rack)
        msg += "\n"
      end

      [ code,
        { 'Content-Type' => 'text/plain', 'Content-Length' => msg.size.to_s },
        [ msg ] ]
    end
  end

  def path_split(env)
    parts = env["PATH_INFO"].sub(%r{/+\z}, "".freeze).split(%r{/+})
    parts.shift # leading slash
    parts
  end

  # some clients may send extra stuff in the input body we ignore, ensure
  # persistent connections stay working
  def drain_input(env)
    input = env["rack.input"]
    bytes = 0
    if buf = input.read(23)
      bytes += buf.size
      while input.read(666, buf)
        bytes += buf.size
      end
      buf.clear
    end
    bytes
  end
end
