# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Mkcol
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def call_mkcol(env)
    bytes = drain_input(env)
    return r(415) if bytes > 0
    parts = path_split(env)
    return r(405) if col_resolve(parts)
    basename = parts.pop or return r(403)
    parent = col_resolve(parts) or return r(409)
    if node = node_lookup(parent[:id], basename)
      return r(403) unless node[:collection]
    else
      node = col_ensure(parent[:id], basename)
    end
    r(201)
  rescue OMGDAV::TypeConflict
    r(409)
  end
end
