# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Delete
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def delete_file(node, cache)
    node_delete(node)
    key = node_to_key(node, cache)
    cache_invalidate(key)
    @mogc.delete(key)
  rescue MogileFS::Backend::UnknownKeyError
    warn "key=#{key.inspect} missing on delete from domain=#{@mogc.domain}"
  end

  def delete_collection(node, cache)
    queue = [ node ]
    q = { domain_id: @domain_id }

    while cur_node = queue.pop
      q[:parent_id] = cur_node[:id]

      begin
        state = :done
        @db[:paths].where(q).limit(@sql_limit).each do |child_node|
          if child_node[:collection]
            queue << cur_node
            queue << child_node
            state = :descend
            break
          else
            delete_file(child_node, cache)
            state = :continue
          end
        end
      end while state == :continue

      node_delete(cur_node) if state == :done
    end
  end

  def delete_entry(node, cache)
    if node[:collection]
      delete_collection(node, cache)
    else
      delete_file(node, cache)
    end
  end

  def call_delete(env)
    # makes litmus happy with the "delete_fragment" test
    # FRAGMENT may be set by Mongrel and descendents (unicorn/thin)
    env["FRAGMENT"] and return r(400)

    parts = path_split(env)
    node = node_resolve(parts) or return r(404)
    node[:parent_id] == 0 and return r(403)
    delete_entry(node, {})

    r(204)
  end
end
