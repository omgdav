# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Move
  include OMGDAV::DB
  include OMGDAV::RackUtil
  include OMGDAV::CopyMove

  def call_move(env)
    src, dst = {}, {}
    err = copy_move_prepare!(env, src, dst) and return err

    dst_node = { name: dst[:basename], parent_id: dst[:parent][:id] }

    if src[:node][:collection]
      case env['HTTP_DEPTH']
      when nil, "infinity"
        move_collection(src[:node], dst_node)
      else
        return r(400, "invalid Depth: #{env['HTTP_DEPTH'].inspect}")
      end
    else
      dst_key = node_to_key(dst_node, {})
      @mogc.rename(src[:key], dst_key)
    end
    @db[:paths].where(id: src[:node][:id]).update(dst_node)
    if dst_key
      cache_invalidate(src[:key])
      cache_invalidate(dst_key)
    else
      cache_invalidate(nil) # just kill everything
    end
    r(dst[:node] ? 204 : 201)
  end

  def move_collection(src_node, dst_node)
    cache_old = {}
    paths = @db[:paths]
    queue = [ [ 0, src_node ] ]
    max_id = paths.max(:id)
    q = { domain_id: @domain_id }

    # poison cache_new with post-MOVE data
    cache_new = {}
    cache_new[src_node[:id]] = node_to_parts(dst_node, {}).freeze

    while cur_job = queue.pop
      min_id, cur_src = cur_job
      q[:parent_id] = cur_src[:id]
      next if min_id == max_id
      begin
        continue = false
        q[:id] = ((min_id+1)..max_id)
        paths.order(:id).where(q).limit(@sql_limit).each do |child_node|
          min_id = child_node[:id]
          if child_node[:collection]
            queue << [ min_id, cur_src ]
            queue << [ 0, child_node ]

            # poison cache_new with post-MOVE data
            tmp = { name: child_node[:name], parent_id: cur_src[:id] }
            cache_new[child_node[:id]] = node_to_parts(tmp, cache_new).freeze

            continue = false
            break
          else
            old = node_to_key(child_node, cache_old)
            new = node_to_key(child_node, cache_new)
            @mogc.rename(old, new)
            continue = true
          end
        end
      end while continue
    end
  end
end
