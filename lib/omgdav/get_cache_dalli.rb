# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>

# Allows optional caching of recent URLs for GET requests
# This can helps with fusedav (and possibly other things) which fetch
# files in many small chunks using partial requests.
require 'dalli'
module OMGDAV::GetCacheDalli
  def self.extended(owner)
    owner.instance_eval do
      @dalli_gen = Process.clock_gettime(Process::CLOCK_MONOTONIC, :second).to_s
      @dalli = Dalli::Client.new("localhost:11211")
    end
  end

  attr_accessor :dalli

  # see omgdav/get
  def resolve_node(key, parts)
    ckey = -"#@dalli_gen#{key}"
    node = @dalli.get(ckey) and return node
    node = super
    @dalli.set(ckey, node)
    node
  rescue Dalli::RingError, Dalli::NetworkError
    node || super
  end

  def cache_invalidate(key)
    if key
      begin
        @dalli.delete(-"#@dalli_gen#{key}")
      rescue Dalli::RingError, Dalli::NetworkError
        @dalli_gen.succ!
      end
    else
      # nuke everything by changing prefix
      @dalli_gen.succ!
    end
  end
end
