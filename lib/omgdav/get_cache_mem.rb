# -*- encoding: binary -*-
# Copyright (C) 2019 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>

# Allows optional in-memory caching of recent URLs for GET requests
# This can helps with fusedav (and possibly other things) which fetch
# files in many small chunks using partial requests.
#
# Extend the OMGDAV::App object with this module if needed.

module OMGDAV::GetCacheMem # :nodoc:
  def self.extended(owner)
    owner.instance_eval do
      @cache = [] # idx => [ key, val ]

      # unconfigurable size :/
      @cache_mask = (1 << 14) - 1
    end
  end

  # see omgdav/get
  def resolve_node(key, parts)
    slot = key.hash & @cache_mask
    if ent = @cache[slot]
      return ent[1] if ent[0] == key
    end
    node = super
    @cache[slot] = [ key, node ]
    node
  end

  def cache_invalidate(key)
    if key
      slot = key.hash & @cache_mask
      @cache[slot] = nil if @cache.dig(slot, 0) == key
    else
      @cache = []
    end
  end
end
