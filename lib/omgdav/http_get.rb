# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2019 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "thread"
require "timeout"
require "kcar"
require 'socket'
require 'io/wait'

class OMGDAV::HttpGet < Kcar::Response # :nodoc:
  # support gzip and partial requests if storage nodes support it
  PASS = %w(Range Accept-Encoding).map! do |x|
    [ "HTTP_#{x.tr('a-z-', 'A-Z_')}", x ]
  end

  DEF_ENV = { "REQUEST_METHOD" => -"GET" }
  ADDR_LOCK = Mutex.new
  ADDR_CACHE = {}
  POOL_LOCK = Mutex.new
  POOL = Hash.new { |pool,ip_port| pool[ip_port] = [] }

  def self.reset
    ADDR_LOCK.synchronize { ADDR_CACHE.clear }
    POOL_LOCK.synchronize { POOL.clear }
  end

  # passed to kcar
  class MySocket < Socket # :nodoc:
    attr_writer :expiry

    def self.start(addr)
      s = new(:INET, :STREAM, 0)
      s.connect_nonblock(addr, exception: false)
      s
    end

    def mono_now
      Process.clock_gettime(Process::CLOCK_MONOTONIC)
    end

    def wait_time
      tout = @expiry ? @expiry - mono_now : @timeout
      raise Timeout::Error if tout < 0
      tout
    end

    def readpartial(bytes, buf = Thread.current[:omgdav_buf] ||= "")
      case read_nonblock(bytes, buf, exception: false)
      when :wait_readable
        wait_readable(wait_time)
      when nil
        raise EOFError, "end of file reached", []
      else # String
        return buf
      end while true
    end

    def start(buf, timeout)
      @timeout = timeout
      @expiry = mono_now + timeout
      wait_writable(wait_time) # wait for connect_nonblock()
      n = buf.size
      case w = write_nonblock(buf, exception: false)
      when :wait_writable
        wait_writable(wait_time)
      else
        return if n == w

        # unlikely retry
        n -= w
        tmp = buf.byteslice(w, n)
        buf.clear
        buf = tmp
      end while true
    end
  end

  # Called by the Rack server at the end of a successful response
  def close
    reusable = @parser.keepalive? && @parser.body_eof?
    super
    POOL_LOCK.synchronize { POOL[@ip_port] << self } if reusable
    nil
  end

  def dispatch(req, timeout)
    @sock.start(req, timeout)
  end

  # returns true if the socket is still alive, nil if dead
  def sock_alive?
    @reused = (:wait_readable == (@sock.read_nonblock(1, exception: false))) ?
              true : @sock.close
  end

  # returns true if the socket was reused and thus retryable
  def fail_retryable?
    @sock.close
    @reused
  end

  def initialize(sock, ip_port)
    super(sock)
    @reused = false
    @ip_port = ip_port
  end

  def self.optional_headers(env)
    PASS.map { |from, to| tmp = env[from] and "#{to}: #{tmp}\r\n" }.join
  end

  def self.run(env, uri, timeout = 2)
    obj = obj_get(uri.host, uri.port)
    env ||= DEF_ENV
    m = env["REQUEST_METHOD"]
    req = "#{m} #{uri.path} HTTP/1.0\r\n" \
          "Connection: keep-alive\r\n" \
          "#{optional_headers(env)}\r\n"
    begin
      obj.dispatch(req, timeout)
      req.clear
      status, header, body = res = obj.rack

      case m
      when "GET"
        body.sock.expiry = nil
      when "HEAD"
        # kcar doesn't know if it's a HEAD or GET response, and HEAD
        # responses have Content-Length in it which fools kcar..
        body.parser.body_bytes_left = 0
        res[1] = header.dup
        body.close # clobbers original header
        res[2] = body = []
      end

      case status.to_i
      when 206, 200, 416
        res
      else
        OMGDAV::BadResponse.new(status.to_s)
      end
    rescue => e
      if obj.fail_retryable?
        obj = obj_get(uri.host, uri.port)
        retry
      end
      return e
    end
  end

  def self.obj_get(ip, port)
    ip_port = "#{ip}:#{port}"
    while obj = POOL_LOCK.synchronize { POOL[ip_port].pop }
      return obj if obj.sock_alive?
    end

    addr = ADDR_LOCK.synchronize do
      ADDR_CACHE[ip_port] ||= Socket.sockaddr_in(port, ip)
    end

    new(MySocket.start(addr), ip_port)
  end

  def stream_to(io)
    each { |buf| io.write(buf) }
    close
  end
end
