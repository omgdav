# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require 'logger'
require 'omgdav'
require 'omgdav/db'
require 'omgdav/http_get'
# :startdoc:

class OMGDAV::App # :nodoc:
  include OMGDAV::DB

  attr_accessor :create_full_put_path
  attr_accessor :new_file_opts
  attr_accessor :get_path_opts

  # Example (for Rack config.ru):
  #
  #   db = Sequel.connect("sqlite://foo.sqlite")
  #   mogc = MogileFS::MogileFS.new(hosts: %w(127.0.0.1:7001), domain: "test")
  #   run OMGDAV::App.new(db, mogc, methods: :worm)
  def initialize(db, mogc, opts = {})
    @call_map = {}
    @db = db
    @mogc = mogc
    @domain_id = ensure_domain(mogc.domain)
    @create_full_put_path = opts[:create_full_put_path] || false
    @new_file_opts = opts[:new_file_opts] || { largefile: :stream }
    @get_path_opts = opts[:get_path_opts] || {}
    @sql_limit = 100
    @root_node = nil

    # avoid contention by loading this first
    root_node
    @db.disconnect # unicorn may load this before forking, don't share
    @worm = false

    ro_methods = %w(GET PROPFIND OPTIONS)
    rw_methods = %w(PUT DELETE MKCOL COPY MOVE PROPPATCH)
    case opts[:methods]
    when nil, :rw
      rmethods = ro_methods + rw_methods
    when :ro
      rmethods = ro_methods
    when :worm
      @worm = true
      rmethods = ro_methods + %w(PUT MKCOL COPY)
    else
      rmethods = opts[:methods]
    end
    rmethods.each do |m|
      m = m.to_s
      require "omgdav/#{m.downcase}"
      extend OMGDAV.const_get(m.capitalize)
      @call_map[m.upcase] = method("call_#{m.downcase}")
    end
    get = @call_map["GET"] and @call_map["HEAD"] = get
    @call_map.default = proc { |_| r(405) }
  end

  def call(env) # :nodoc:
    @call_map[env["REQUEST_METHOD"]].call(env)
  rescue => e
    l = logger(env)
    l.error("#{e.message} (#{e.class})")
    e.backtrace.each { |line| l.error(line) }
    r(500)
  end

  def cache_invalidate(key)
    # noop, see get_cache_*
  end
end
