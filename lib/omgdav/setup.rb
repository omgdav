# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# This is the code behind omgdav-setup(1)
require "omgdav"

module OMGDAV::Setup
  def self.run(argv = ARGV.dup)
    migdir = "#{File.dirname(__FILE__)}/migrations"
    Sequel.extension :migration, :core_extensions
    db = ARGV.shift
    db = Sequel.connect(db)
    Sequel::Migrator.apply(db, migdir, nil)
  end
end
