# -*- encoding: binary -*-
# Copyright (C) 2019 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
#
# Extend the MogileFS::MogileFS object with this module

module OMGDAV::MogcCacheMem # :nodoc:
  def self.extended(mfsc)
    mfsc.instance_eval do
      @omgdav_cache = [] # idx => [ key, val ]
      @omgdav_cache_mask = (1 << 14) - 1
    end
  end

  def get_uris(key, *opts)
    slot = key.hash & @omgdav_cache_mask
    if ent = @omgdav_cache[slot]
      return ent[1] if ent[0] == key
    end
    rv = super
    @omgdav_cache[slot] = [ -key, rv ]
    rv
  end

  def _omg_invalidate(key)
    slot = key.hash & @omgdav_cache_mask
    @omgdav_cache[slot] = nil if @omgdav_cache.dig(slot, 0) == key
  end

  def new_file(key, *opts)
    super
  ensure
    _omg_invalidate(-key)
  end

  def rename(old, new)
    super
  ensure
    _omg_invalidate(-old)
    _omg_invalidate(-new)
  end
end
