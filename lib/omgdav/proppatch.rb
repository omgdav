# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"
require "omgdav/input_wrapper"

module OMGDAV::Proppatch
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def value(z)
    z.children.text.strip
  end

  def set_props(node, x)
    props = nil
    x.children.each do |y|
      next unless Nokogiri::XML::Element === y
      next unless "prop" == y.name
      y.children.each do |z|
        next unless Nokogiri::XML::Element === z
        ns = z.namespace.href rescue ""
        case ns
        when "DAV:"
          case z.name
          when "getcontenttype"
            node[:contenttype] = content_type_id(value(z))
          when "getlastmodified"
            mtime = value(z)
            node[:mtime] = Time.httpdate(mtime).to_i
          when "creationdate"
            created = value(z)
            node[:created] = Time.iso8601(created).to_i
          end
        when OMGDAV::LP2
          case z.name
          when "executable"
            node[:executable] = ("T" == z.children.to_a.join.strip)
          end
        else
          props ||= dead_props_get(node) || {}
          nsprops = props[ns] ||= {}
          remove_namespace!(x)
          nsprops[z.name] = z.children.to_xml
        end
      end
    end
    dead_props_set(node, props) if props
  end

  def remove_props(node, x)
    props = nil
    x.children.each do |y|
      next unless Nokogiri::XML::Element === y
      next unless "prop" == y.name
      y.children.each do |z|
        next unless Nokogiri::XML::Element === z
        ns = z.namespace.href rescue ""
        case ns
        when "DAV:"
          case z.name
          when "getcontenttype"
            node[:contenttype] = nil
          end
        when OMGDAV::LP2
          case z.name
          when "executable"
            node[:executable] = false
          end
        else
          props ||= dead_props_get(node) or next
          nsprops = props[ns] or next
          nsprops.delete(z.name) or next
        end
      end
    end
    dead_props_set(node, props) if props
  end

  def call_proppatch(env)
    parts = path_split(env)
    node = node_resolve(parts) or return r(404)
    input = OMGDAV::InputWrapper.new(env)
    xml = Nokogiri::XML(input)
    xmlns = xml.root.namespace
    return r(400) unless "DAV:" == xmlns.href
    xml.xpath("//D:propertyupdate", "D" => "DAV:").children.each do |x|
      next unless Nokogiri::XML::Element === x

      case x.name
      when "set"
        set_props(node, x)
      when "remove"
        remove_props(node, x)
      else
        return r(400, "Unknown name=#{x.name}")
      end
    end
    @db[:paths].where(id: node.delete(:id)).update(node)
    cache_invalidate(parts.join('/'.freeze))
    r(200)
  rescue Nokogiri::SyntaxError => e
    r(400, "syntax error #{e.message}")
  rescue OMGDAV::InvalidContentType => e
    r(400, "bad getcontenttype: #{e.message}")
  end

  # removes namespace recursively without operating recursively
  def remove_namespace!(el)
    queue = [ el ]
    while el = queue.shift
      el.namespace = nil if el.respond_to?(:namespace=)
      queue.concat(el.children.to_a) if el.respond_to?(:children)
    end
  end
end
