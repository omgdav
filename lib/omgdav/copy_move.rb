# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"
require "omgdav/delete"

module OMGDAV::CopyMove
  include OMGDAV::DB
  include OMGDAV::RackUtil
  include OMGDAV::Delete

  def copy_move_prepare!(env, src, dst)
    dst_uri = env["HTTP_DESTINATION"] or return r(400)
    dst_uri = URI(dst_uri)
    req_uri = URI(Rack::Request.new(env).url)
    return r(502) if dst_uri.host != req_uri.host
    return r(502) if dst_uri.port != req_uri.port

    script_name = Regexp.quote(env["SCRIPT_NAME"])
    dst_path = dst_uri.path.dup
    return r(502) unless dst_path.gsub!(%r{\A#{script_name}/}, '/'.freeze)

    dst[:parts] = path_split("PATH_INFO" => dst_path)
    dst[:basename] = dst[:parts].pop
    dst[:parent] = col_resolve(dst[:parts]) or return r(409)
    dst[:node] = node_lookup(dst[:parent][:id], dst[:basename])
    dst[:parts] << dst[:basename]
    dst[:key] = dst[:parts].join('/'.freeze)

    case env["HTTP_OVERWRITE"]
    when "F"
      dst[:node] and return r(412)
    when "T", nil
      (@worm && dst[:node]) and return r(409)
    else
      return r(400, "Overwrite: #{env['HTTP_OVERWRITE']} not understood")
    end

    src[:parts] = path_split(env)
    src[:basename] = src[:parts].pop
    src[:parent] = col_resolve(src[:parts]) or return r(404)
    src[:parts] << src[:basename]
    src[:node] = node_lookup(src[:parent][:id], src[:basename]) or return r(404)
    src[:key] = src[:parts].join('/'.freeze)

    return r(403) if src[:key] == dst[:key]

    delete_entry(dst[:node], {}) if dst[:node]

    nil
  end
end
