# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/db"

class OMGDAV::PropfindResponse # :nodoc:
  include OMGDAV::DB

  # This is a Rack response body for HTTP/1.0 folks who can't handle
  # Transfer-Encoding: chunked
  class XMLTmp < Tempfile # :nodoc:
    def close
      super true # unlink immediately
    end

    # no line buffering
    def each
      buf = Thread.current[:omgdav_xmltmp_rbuf] ||= ""
      while read(16384, buf)
        yield buf
      end
    end
  end

  def initialize(env, parent, db)
    @env = env
    @parent = parent
    @db = db
    @n2k_cache = {}
    @domain_id = parent[:domain_id]
    @script_name = env['SCRIPT_NAME']
    @ct_cache = {}
  end

  def response
    headers = { "Content-Type" => 'text/xml; charset="utf-8"' }
    case @env["HTTP_VERSION"]
    when "HTTP/1.1"
      headers["Transfer-Encoding"] = "chunked"
      body = self
    else
      body = XMLTmp.new("omgdav_propfind")
      body.sync = true
      each_blob { |blob| body.write(blob) }
      headers["Content-Length"] = body.size.to_s
      body.rewind
    end

    [ 207, headers, body ]
  end

  # chunks a Rack response body for HTTP/1.1
  def each
    each_blob do |blob|
      yield "#{blob.bytesize.to_s(16)}\r\n#{blob}\r\n"
    end
    yield "0\r\n\r\n"
  end

  def row_xml(x, row)
    is_col = row[:collection]
    props = dead_props_get(row)
    if props
      prop_ns = 'xmlns:ns0="DAV" '
      i = 0
      props_str = ""
      props.keys.sort.each do |ns|
        if ns.empty?
          pfx = ""
        else
          pfx = "ns#{i += 1}"
          prop_ns << "xmlns:#{pfx}=\"#{ns}\" "
          pfx << ":"
        end
        props[ns].each do |name,value|
          # no need to escape value here because we never escaped it on the
          # way into the DB(!)
          props_str << "<#{pfx}#{name}>#{value}</#{pfx}#{name}>"
        end
      end
    end

    x << %Q(<D:response #{prop_ns}xmlns:lp1="DAV:" xmlns:lp2="#{OMGDAV::LP2}">)
    name = node_to_key(row, @n2k_cache)
    name.encode!(xml: :text)
    name << "/" if is_col && ! row[:name].empty?

    x << "<D:href>#@script_name/#{name}</D:href>"

    x << "<D:propstat>"
    x << "<D:prop>"

    if is_col
      x << "<lp1:resourcetype><D:collection/></lp1:resourcetype>"
    else
      x << "<lp1:resourcetype/>"
      x << "<lp2:executable>#{row[:executable] ? 'T' : 'F'}</lp2:executable>"
      x << "<lp1:getcontentlength>#{row[:length]}</lp1:getcontentlength>"
      getcontenttype = content_type(row, @ct_cache)
      x << "<lp1:getcontenttype>#{getcontenttype}</lp1:getcontenttype>"
    end

    if props_str
      x << props_str
      props_str.clear
      prop_ns.clear
    end

    created = Time.at(row[:created]).utc.xmlschema
    x << "<lp1:creationdate>#{created}</lp1:creationdate>"
    mtime = Time.at(row[:mtime]).httpdate
    x << "<lp1:getlastmodified>#{mtime}</lp1:getlastmodified>"
    x << "</D:prop>"
    x << "<D:status>HTTP/1.1 200 OK</D:status>"
    x << "</D:propstat>"
    x << "</D:response>"
  end

  def each_blob
    paths = @db[:paths]
    q = { parent_id: @parent[:id], domain_id: @parent[:domain_id] }
    prev = { name: "" }
    x = '<?xml version="1.0" encoding="utf-8"?>' \
        '<D:multistatus xmlns:D="DAV:">'

    # FIXME: this might be horribly inefficient
    case @env["HTTP_DEPTH"]
    when "0"
      row_xml(x, @parent)
    when "1"
      if @parent[:collection]
        row_xml(x, @parent)
        begin
          seen = 0
          paths.order(:name).where {
            self.&(q, self.>(:name, prev[:name]))
          }.limit(@sql_limit).each do |row|
            seen += 1
            prev = row
            row_xml(x, row)
          end

          break if seen != @sql_limit
          yield x
          x.clear
        end while true
      else
        row_xml(x, @parent)
      end
    end

    x << '</D:multistatus>'
    yield x
  end
end
