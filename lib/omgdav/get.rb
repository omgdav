# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Get
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def resolve_node(key, parts)
    basename = parts.pop
    parent = col_resolve(parts) or return
    parts << basename
    node_lookup(parent[:id], basename)
  end

  def call_get(env)
    parts = path_split(env)
    key = -parts.join('/'.freeze)
    node = resolve_node(key, parts) or return r(404)
    @mogc.get_uris(key, @get_path_opts).each do |uri|
      case res = OMGDAV::HttpGet.run(env, uri)
      when Array
        headers = res[1]
        headers["Last-Modified"] = -Time.at(node[:mtime]).httpdate
        headers["Content-Type"] = content_type(node)
        return res
      else
        logger(env).error("#{uri}: #{res.message} (#{res.class})")
      end
    end
    r(500)
  rescue MogileFS::Backend::UnknownKeyError,
         MogileFS::Backend::DomainNotFoundError
    r(404, "")
  end
end
