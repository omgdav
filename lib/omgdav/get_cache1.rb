# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>

# Allows optional caching of the most-recently-read URL for GET requests
# This can helps with fusedav (and possibly other things) which fetch
# files in many small chunks using partial requests.
module OMGDAV::GetCache1
  def self.extended(owner)
    owner.instance_eval do
      @last_lock = Mutex.new
      @last_key = nil
      @last_node = nil
    end
  end

  # see omgdav/get
  def resolve_node(key, parts)
    @last_lock.synchronize do
      return @last_node if @last_key == key
    end
    node = super
    @last_lock.synchronize do
      @last_key = key
      @last_node = node
    end
  end

  def cache_invalidate(key)
    @last_key = nil # no need to lock for assignment
  end
end
