# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Put
  include OMGDAV::DB
  include OMGDAV::RackUtil

  def new_file_prepare(env)
    params = Rack::Utils.parse_query(env["QUERY_STRING"])

    # prepare options for create_open/create_close:
    new_file_opts = @new_file_opts.dup
    new_file_opts[:class] = params["class"] || "default"

    # try to give a Content-Length to the tracker
    clen = env["CONTENT_LENGTH"]
    clen and new_file_opts[:content_length] = clen.to_i

    if /\bContent-MD5\b/i =~ env["HTTP_TRAILER"]
      # if the client will give the Content-MD5 as the trailer,
      # we must lazily populate it since we're not guaranteed to
      # have the trailer, yet (rack.input is lazily read on unicorn)
      new_file_opts[:content_md5] = lambda { env["HTTP_CONTENT_MD5"] }
    elsif cmd5 = env["HTTP_CONTENT_MD5"]
      # maybe the client gave the Content-MD5 in the header
      new_file_opts[:content_md5] = cmd5
    end

    new_file_opts
  end

  def edit_input(tmp, input, off_out)
    tmp.seek(off_out)
    IO.copy_stream(input, tmp)
    tmp.rewind
    tmp
  end

  def call_put(env)
    return r(403) if %r{/\z} =~ env["PATH_INFO"]
    return r(100) if %r{\b100-continue\b}i =~ env["HTTP_EXPECT"]
    parts = path_split(env)
    key = parts.join('/'.freeze)
    basename = parts.pop or return r(403)

    if @create_full_put_path
      parent = col_vivify(parts)
    else
      parent = col_resolve(parts) or return r(404)
    end

    node = node_lookup(parent[:id], basename)
    return r(409) if node && @worm

    if range = env["HTTP_CONTENT_RANGE"]
      %r{\A\s*bytes\s+(\d+)-(\d+)/\*\s*\z} =~ range or
        return r(400, "Bad range", env)
      clen = env["CONTENT_LENGTH"] or
        return r(400, "Content-Length required for Content-Range")
      off_out = $1.to_i
      len = $2.to_i - off_out + 1
      len == clen.to_i or
             return r(400,
                      "Bad range, Content-Range: #{range} does not match\n" \
                      "Content-Length: #{clen.inspect}", env)
      tmp = Tempfile.new('put_cr')
      tmp.sync = true
    end

    input = env["rack.input"]
    if node
      if tmp
        @mogc.get_uris(key, @get_path_opts).each do |uri|
          case res = OMGDAV::HttpGet.run(nil, uri)
          when Array
            res[2].stream_to(tmp)
            input = edit_input(tmp, input, off_out)
            break
          else
            logger(env).error("#{uri}: #{res.message} (#{res.class})")
          end
        end
        input or return r(500, "Could not retrieve key=#{key.inspect}")
      end
    else
      input = edit_input(tmp, input, off_out) if tmp && off_out != 0
    end

    # finally, upload the file
    new_file_opts = new_file_prepare(env)
    length = @mogc.new_file(key, new_file_opts) do |io|
      IO.copy_stream(input, io)
    end
    info = { "length" => length }

    if node
      node_update(node, info)
      cache_invalidate(key)
      r(204)
    else
      file_ensure(parent[:id], basename, info)
      r(201)
    end
  ensure
    tmp.close! if tmp
  end
end
