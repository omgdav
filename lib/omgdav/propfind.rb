# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/rack_util"
require "omgdav/db"
require "omgdav/propfind_response"
require "omgdav/input_wrapper"

module OMGDAV::Propfind
  def call_propfind(env)
    input_validate_propfind(env)
    parts = path_split(env)
    depth = env["HTTP_DEPTH"]
    case depth
    when "0", "1"
      # resolve the collection
      node = node_resolve(parts)

      if node
        OMGDAV::PropfindResponse.new(env, node, @db).response
      else
        r(404)
      end
    else
      return r(400, "Depth: #{depth} not supported")
    end
  rescue Nokogiri::SyntaxError => e
    r(400, "syntax error: #{e.message}")
  end

  def input_validate_propfind(env)
    input = OMGDAV::InputWrapper.new(env)
    parser = Nokogiri::XML::SAX::Parser.new(Validator.new)
    parser.parse_io(input)
  rescue Nokogiri::SyntaxError
    # don't choke on empty input
    raise if input.bytes != 0
  end

  class Validator < Nokogiri::XML::SAX::Document
    def error(string)
      raise Nokogiri::SyntaxError, string
    end
  end
end
