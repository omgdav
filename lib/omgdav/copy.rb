# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
require "omgdav/copy_move"
require "omgdav/rack_util"
require "omgdav/db"

module OMGDAV::Copy
  include OMGDAV::DB
  include OMGDAV::RackUtil
  include OMGDAV::CopyMove

  def call_copy(env)
    src, dst = {}, {}
    err = copy_move_prepare!(env, src, dst) and return err

    if src[:node][:collection]
      case env["HTTP_DEPTH"]
      when "0"
        dst_node = col_ensure(dst[:parent][:id], dst[:basename])
      when nil, "infinity"
        dst_node = col_ensure(dst[:parent][:id], dst[:basename])
        copy_collection(src[:node], dst_node)
      else
        r(400, "invalid Depth: #{env['HTTP_DEPTH']}")
      end
    else
      copy_to_col(src[:node], dst[:parent], {}, dst[:basename])
    end

    # overwrite or new file?
    r(dst[:node] ? 204 : 201)
  end

  # recursively copies src_node to dst_node
  def copy_collection(src_node, dst_node)
    cache = {}
    paths = @db[:paths]
    queue = [ [ 0, src_node, dst_node ] ]
    max_id = paths.max(:id)
    q = { domain_id: @domain_id }

    while cur_job = queue.pop
      min_id, cur_src, dst_col = cur_job
      q[:parent_id] = cur_src[:id]
      next if min_id == max_id
      begin
        continue = false
        q[:id] = ((min_id+1)..max_id)
        paths.order(:id).where(q).limit(@sql_limit).each do |child_node|
          min_id = child_node[:id]
          if child_node[:collection]
            queue << [ min_id, cur_src, dst_col ]
            dst_parent = col_ensure(dst_col[:id], child_node[:name])
            queue << [ 0, child_node, dst_parent ]
            continue = false
            break
          else
            copy_to_col(child_node, dst_col, cache)
            continue = true
          end
        end
      end while continue
    end
  end

  def copy_to_col(src_file, dst_col, cache, dst_name = src_file[:name])
    src_key = node_to_key(src_file, cache)

    dst_parts = node_to_parts(dst_col, cache) << dst_name
    dst_key = dst_parts.join('/'.freeze)

    opts = @new_file_opts.dup
    info = @mogc.file_info(src_key)
    opts[:content_length] = src_file[:length]
    opts[:class] = info["class"]
    checksum = info["checksum"] and opts[:checksum] = checksum
    bytes = @mogc.new_file(dst_key, opts) do |io|
      @mogc.get_uris(src_key, @get_path_opts).each do |uri|
        case res = OMGDAV::HttpGet.run(nil, uri)
        when Array
          res[2].stream_to(io)
          break
        else
          logger(env).error("#{uri}: #{res.message} (#{res.class})")
        end
      end
    end

    if src_file[:length] != bytes
      warn "length mismatch copying(#{src_key.inspect}>#{dst_key.inspect})" \
           "(expected(#{src_file[:length]}) != copied(#{bytes}))"
    end

    info = { "length" => bytes }
    dst_node = file_ensure(dst_col[:id], dst_name, info)
    dst_node
  end
end
