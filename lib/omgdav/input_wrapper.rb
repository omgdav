# -*- encoding: binary -*-
# :enddoc:
# Copyright (C) 2012-2017 all contributors <omgdav-public@bogomips.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
class OMGDAV::InputWrapper # :nodoc:
  attr_reader :bytes

  def initialize(env, max = 65536)
    @input = env["rack.input"]
    @bytes = 0
    @max = max
  end

  def read(*args)
    rv = @input.read(*args)
    if String === rv
      @bytes += rv.size
      if @bytes > @max
        buf = ""
        while @input.read(16384, buf)
          # drain input
        end
        buf.clear
        raise "too many bytes for XML: #@bytes > #@max"
      end
    end
    rv
  end

  # no-op to fool Nokogiri
  def close
  end
end
